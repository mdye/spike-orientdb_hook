package com.sungardas.cloud.devops.orientdb.notify

import akka.actor.Actor
import spray.can.Http
import spray.http.HttpMethods._
import spray.http.HttpRequest
import spray.http.HttpResponse
import spray.http.Uri
import spray.http.HttpEntity
import akka.event.Logging
import akka.actor.ActorLogging

class NotificationListener extends Actor with ActorLogging {
  
  def receive = {
    case _: Http.Connected => sender ! Http.Register(self)
    
    case HttpRequest(POST, Uri.Path("/notifications"), headers, entity: HttpEntity.NonEmpty, protocol) =>
      log.info(s"Received doc: $entity")
      sender ! HttpResponse()
      
    case _: HttpRequest => sender ! HttpResponse(status = 404, entity = "Not Found")
  }
}