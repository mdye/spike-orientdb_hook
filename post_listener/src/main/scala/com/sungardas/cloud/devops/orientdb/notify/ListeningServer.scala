package com.sungardas.cloud.devops.orientdb.notify

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.io.IO
import spray.can.Http

object ListeningServer extends App {
  implicit val system = ActorSystem("post-listener")
  val notificationListener: ActorRef = system.actorOf(Props[NotificationListener], name="notificationListener")
  
  IO(Http) ! Http.Bind(notificationListener, interface = "0.0.0.0", port=scala.util.Properties.envOrElse("PORT", "8099").toInt) 
}