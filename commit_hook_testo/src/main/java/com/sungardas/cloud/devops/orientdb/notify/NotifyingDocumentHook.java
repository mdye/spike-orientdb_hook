package com.sungardas.cloud.devops.orientdb.notify;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.orientechnologies.orient.core.hook.ODocumentHookAbstract;
import com.orientechnologies.orient.core.record.impl.ODocument;

/**
 * <p></p>
 * 
 * @author mdye
 *
 */
public class NotifyingDocumentHook extends ODocumentHookAbstract {

  static final Logger LOG = Logger.getLogger("NotifyingDocumentHook");

  @Override
  public DISTRIBUTED_EXECUTION_MODE getDistributedExecutionMode() {
    return DISTRIBUTED_EXECUTION_MODE.BOTH;
  }

  @Override
  public RESULT onRecordBeforeCreate(final ODocument doc) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
    String now = formatter.format(new Date());
    
    // write new field
    doc.field("created_at", now);
    LOG.info(String.format("Created new doc at %s", now));
    
    return RESULT.RECORD_CHANGED;
  }
  
  @Override
  public void onRecordAfterCreate(final ODocument doc) {
    DocChangeNotifier$.MODULE$.notify(doc.toJSON());
  }

  @Override
  public void onRecordAfterUpdate(final ODocument doc) {
    DocChangeNotifier$.MODULE$.notify(doc.toJSON());
  }

  @Override
  public void onRecordAfterDelete(final ODocument doc) {
    DocChangeNotifier$.MODULE$.notify(doc.toJSON());
  }
}