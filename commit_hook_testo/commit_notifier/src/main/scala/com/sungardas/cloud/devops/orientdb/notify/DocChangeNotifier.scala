package com.sungardas.cloud.devops.orientdb.notify

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import javax.print.Doc
import scalaj.http.Http

object DocChangeNotifier  {
  
  val POSTEndpoint = "http://localhost:8099/notifications"
  
  val publisher = ActorSystem("doc-publisher")
  val POSTer = publisher.actorOf(Props(new DocPOSTer), "POSTer")
  
  /**
   * Receives ODocuments from db hooks, publishes using publishing system.
   */
  def notify(doc: String): Unit = {
    POSTer ! new Doc(doc)
  }
 
  case class Doc(content: String)
  case object Sent
  
  class DocPOSTer extends Actor with ActorLogging {
    
    /**
     * Actor receives doc, delegates to doPost
     */
    def receive = {
      case Doc(doc: String) => 
        // log.info("Received doc: " + doc)
        doPost(doc)
    }
    
    /**
     * Send doc via HTTP POST
     */
    def doPost(doc: String) = {
      Http.postData(POSTEndpoint, doc).header("content-type", "application/json").responseCode
    }
  }
}