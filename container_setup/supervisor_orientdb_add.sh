#!/bin/sh

ORIENTDB_HOME=$(ls -d /opt/orientdb-latest/releases/orientdb-community-*)

cat >> /etc/supervisord.conf <<EOF

[program:orientdb]
command=$ORIENTDB_HOME/bin/server.sh
autostart=true
autorestart=true
stderr_logfile=/var/log/orientdb.err.log
stdout_logfile=/var/log/orientdb.out.log
EOF
