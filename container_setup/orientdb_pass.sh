#!/bin/sh

ORIENTDB_HOME=$(ls -d /opt/orientdb-latest/releases/orientdb-community-*)

yum install -y xmlstarlet
xmlstarlet ed --inplace -d /orient-server/users/user -s /orient-server/users -t elem -n user -v '' -i /orient-server/users/user -t attr -n resources -v '*'  -i /orient-server/users/user -t attr -n name -v root  -i /orient-server/users/user -t attr -n password -v devo $ORIENTDB_HOME/config/orientdb-server-config.xml
