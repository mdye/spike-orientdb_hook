var NUMINSERTS = 10000;
var QUEUESIZE = 30;
var ORIENTOPOOLSIZE = 20;

var fs = require('fs');
var date = new Date();
var performanceNow = require('performance-now');
var util = require('util');
var semaphore = require('node-semaphore');
var oriento = require('oriento');
var uuid = require('node-uuid');

var server = oriento({
  host: 'localhost',
  port: 2424,
  username: 'root',
  password: 'devo',
  pool: {
    max: ORIENTOPOOLSIZE
  }
});

server.create({
  name: uuid.v4(),
  type: 'graph',
  storage: 'plocal',
}).then(function (db) {
  function writeResults(r, filename) {
    fs.writeFile(filename, JSON.stringify(r), function (err) {
      if (err) {
        console.log('problem saving results');
        process.exit(-1);
      } else {
        console.log('results saved to '+filename);
      }

      console.log('Finished, exiting.')
      process.exit(0);
    });
  }

  function singleInsert(secNum, startTime, results, cb) {
    db.insert().into('V').set({1: 2}).one().then(function(a) {
      var endTime = performanceNow();
      results.push([secNum, startTime, endTime, a['@rid']]);
      cb();
      if (results.length == NUMINSERTS) {
        writeResults(results, '/tmp/orient_results.json');
      }
    });
  }

  function insertWorker(results, recNum, cb) {
    var startTime = performanceNow();
    singleInsert(recNum, startTime, results, cb);
  }

  var results = [];

  var pool = semaphore(QUEUESIZE);

  for (var ix=1; ix<=NUMINSERTS; ix++) {
    (function(recNum) {
      pool.acquire(function() {
        if (recNum % 1000 == 0) {
          console.log('submitted record number ' + recNum)
        }
        insertWorker(results, recNum, function() { pool.release() })

      })
    })(ix);
  }

});

