## Demo ideas

The purpose of this spike is two-fold: first, it provides basic performance testing of DB inserts from a Node.js application; second, it contains an investigatory OrientDB commit hook project (and companion HTTP listener project). The performance testing tool can be used with the hook enabled or disabled.

### Docker installation

See http://docs.docker.io/en/latest/installation/.

### Container setup

Build orientdb_hook container:

    docker build --rm -t mdye/orientdb_hook:vanilla .

### Demo execution

#### Preconditions
Build container and start it (the command isn't necessary if you haven't committed a container started with a different run command than that specified at the end of the Dockerfile):

    docker run -d --name orientdb_hook -p 8099:8099 -p 2480:2480 -p 3324:22 mdye/orientdb_hook:vanilla /usr/bin/supervisord -n -c /etc/supervisord.conf

SSH into container (password is 'devo'):

    ssh -oPort=3324 root@localhost -oPubKeyAuthentication=no

#### Demo Part 1: performance testing OrientDB

In-container, execute:

    /src/perf_tests/benchmark_orientdb.sh

#### Demo Part 2: installing the db commit hook

Deploy commit hook:

    cd /src && gradle deploy

Restart database (just shutting it down is good enough, supervisorctl will restart it):

    $ORIENTDB_HOME/bin/shutdown.sh

Start POST listener:

    cd /src && gradle oneJar && java -jar $(ls ./post_listener/build/libs/*.jar)

Start benchmark_orientdb.sh as described in Part 1. You should run the benchmark tool a few times to get stable results: each end of the notification system will get faster after a few runs as the Java hotspot compiler optimizes the bytecode.

##### Performance testing the spray + akka web notification listener

The notification listener project is almost as trivial as a web application can get. Even still, I needed it to be performant enough to keep up with whatever tests we wanted to do with OrientDB commit hooks. To test the performance of the listener project alone, start the container, start the POST listener and execute:

    /src/perf_tests/benchmark_notification_listener.sh

### Dev environment

#### Container customization

(optional) Given server ssh keys in ~/.ssh/servers/testo/, execute the following to customize the container and re-tag it `mdye/orientdb_hook:vanilla` (note that if you have a running instance of mdye/orientdb_hook:vanilla, you must stop and remove it first):

    tar -cf - -C ~/.ssh/servers/testo/ . | docker run -i mdye/orientdb_hook:vanilla sh -c '(cd /etc/ssh/ && tar -xpf -)' && (CID=$( docker ps -a | grep 'mdye/orientdb_hook' | cut -d' ' -f1 ); docker commit $CID mdye/orientdb_hook:vanilla; docker rm $CID )

(optional) Given a client ssh pubkey in ~/.ssh/id_rsa.pub, execute the following to customize the container and tag it `mdye/orientdb_hook:mykey`:

    cat ~/.ssh/id_rsa.pub | docker run -i mdye/orientdb_hook:vanilla sh -c 'cat > /root/.ssh/authorized_keys && chmod 600 /root/.ssh/authorized_keys' && (CID=$( docker ps -a | grep 'mdye/orientdb_hook' | cut -d' ' -f1 ); docker commit $CID mdye/orientdb_hook:mykey; docker rm $CID )

#### Dev container use

SSH to the container:

    ssh -oPort=3324 root@localhost

... or if you set up a key as in the example above:

    ssh -oPort=3324 root@localhost -i ~/.ssh/id_rsa

(TODO: expand)

### Misc. docker commands

See all containers (running and not):

    docker ps -a

Stop and remove a container by ID:

    docker stop <running_container_id> && docker rm $_

See all images:

    docker images

Stop and remove an image:

    docker rmi <image_id>

For more information about docker commands, see: http://docs.docker.io/en/latest/reference/commandline/

### Notes
* I use supervisord to start the SSH service in the orientdb_hook container when used as a dev environment. This can easily be added-to with other persistent services.

### Known issues
* The SSH key setup procedure is a bit crufty and the details could be hidden in a script that does condition checking before executing. Fix that.
