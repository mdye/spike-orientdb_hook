FROM centos:6.4
MAINTAINER mdye <michael.dye@sungard.com>

# from http://docs.docker.io/en/latest/examples/nodejs_web_app/
RUN rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
RUN yum install -y git java-1.7.0-openjdk-devel unzip npm

RUN cd / && git clone https://bitbucket.org/mdye/docker-container_setup.git
RUN /docker-container_setup/centos/ssh_setup.bash
RUN /docker-container_setup/centos/supervisor_setup.bash
RUN /docker-container_setup/centos/vim_setup.bash
RUN /docker-container_setup/centos/screen_setup.bash
RUN /docker-container_setup/centos/wrk_setup.bash

RUN echo 'root:devo' | chpasswd

# need to remove this pam module for ssh sessions to work in later distributions
RUN sed -i -e 's/^\(session\s\+required\s\+pam_loginuid.so$\)/#\1/' /etc/pam.d/sshd

# fetching, installing ant
RUN cd /usr/local && curl -s http://mirror.nexcess.net/apache/ant/binaries/apache-ant-1.9.3-bin.tar.gz | tar xz --transform='s/apache-ant-[0-9\.]*/ant/' --show-transformed -f - && ln -s /usr/local/ant/bin/ant /usr/local/bin/

# fetching, building orientdb
RUN cd /opt && mkdir orientdb-latest && cd $_ && git clone https://github.com/nuvolabase/orientdb.git && cd orientdb && git checkout -b develop
# a repeatable step
RUN cd /opt/orientdb-latest/orientdb && git pull origin develop && ant clean installg

# add container_setup scripts and configs
ADD container_setup /container_setup

# set up envvars
RUN /container_setup/envvars.sh

# add orientdb start to supervisord
RUN /container_setup/supervisor_orientdb_add.sh

# mod orientdb config
RUN /container_setup/orientdb_pass.sh

# fetching, installing gradle
RUN cd /usr/local && curl -s -o /tmp/gradle.zip http://downloads.gradle.org/distributions/gradle-1.11-bin.zip && unzip -d `pwd` /tmp/gradle.zip && ln -s $(ls /usr/local/gradle-*/bin/gradle) /usr/local/bin/

# N.B. This is merely for convenient demo-running; dev use cases update content of /src over ssh
ADD . /src

# install nodejs packages
RUN cd /src/perf_tests/ && npm install inherits node-uuid oriento performance-now node-semaphore

# N.B. need to run this manually in containers that have been committed after usind "docker run" with other commands
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
